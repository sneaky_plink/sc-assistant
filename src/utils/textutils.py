from typing import Union, List
import unicodedata as ud
from tkinter import Tk

from src.utils.formats import ISO_639_1_LANGUAGES


def make_tag_list(genres: List[str], mediainfo: dict, year: Union[int, str], original_language: str, release_group: Union[str, None]) -> List[str]:
    """Generates a list of relevant tags for the description, depending on a number of factors

    Args:
        genres (List[str]): Genres of the media
        mediainfo (dict): Helpful information from the earlier mediainfo dump
        year (Union[int, str]): Year(s) of the media
        original_language (str): Original language of the media
        release_group (Union[str, None]): Release group, if any

    Returns:
        List[str]: All tags that were determined to be of value
    """
    tags = []
    tags += genres
    if original_language != 'en':
        tags.append(ISO_639_1_LANGUAGES[original_language])
        subs = mediainfo['text']
        if len(set([x['language'] for x in subs])) > 1:
            tags.append('multi.sub')
        elif 'en' in [x['language'] for x in subs]:
            tags.append('english.sub')
    tags += [
        mediainfo['general']['format'],
        mediainfo['video']['codec'],
        mediainfo['video']['common_resolution'],
        mediainfo['audio'][0]['codec'],
        mediainfo['audio'][0]['channels']
    ]
    unique_audio_languages = len(
        set(x['language'] for x in mediainfo['audio']))
    if unique_audio_languages > 2:
        tags.append('multi.audio')
    elif unique_audio_languages == 2:
        tags.append('dual.audio')
    if isinstance(year, int) or isinstance(year, str) and '-' not in year:
        tags.append(str(year))
        tags.append(f'{int(year) // 10 * 10}s')
    elif isinstance(year, str):
        split_years = year.split('-')
        start_decade = int(split_years[0]) // 10 * 10
        end_decade = int(split_years[1]) // 10 * 10
        if start_decade == end_decade:
            tags.append(f'{start_decade}s')
        else:
            current_decade = start_decade
            while current_decade <= end_decade:
                tags.append(f'{current_decade}s')
                current_decade += 10
    if release_group:
        tags.append(release_group)

    return [str(x) for x in tags]


def make_title(title: str, original_title: str, alternate_title: str, year: str, edition: Union[str, None], original_language: Union[str, None], seasons: List[int], complete: bool, type: str, tmdb_seasons: int) -> str:
    """Creates a CRT compliant title based on all of the given parameters

    Args:
        title (str): The title as provided TMDB
        original_title (str): Original title, used for non-english content
        alternate_title (str): Alternate title, for english content if it had or 
            has a different name
        year (str): Year(s) of the media
        edition (Union[str, None]): Edition of the media (i.e., criterion)
        original_language (Union[str, None]): Original language of the media
        seasons (List[int]): Seasons that are present in the files provided
        complete (bool): Whether or not we believe this is a complete series
        type (str): Either movie or tv, the category to upload in
        tmdb_seasons (int): The number of seasons listed by TMDB

    Returns:
        str: The full title string to be used
    """
    tv = False
    multi_season = False
    non_english = False
    if isinstance(edition, list):
        edition = ' '.join(edition)
    if type == 'tv':
        tv = True
        if seasons == [-1]:
            if tmdb_seasons > 1:
                seasons = [1, tmdb_seasons]
            else:
                seasons = [1]
    if len(seasons) > 1:
        multi_season = True
    if original_language != 'en':
        non_english = True
    if original_title and non_english and title.lower() != original_title.lower() and is_latin_char(original_title):
        title_string = f'{title} AKA {original_title}'
    elif alternate_title and not non_english and title.lower() != alternate_title.lower() and is_latin_char(alternate_title):
        title_string = f'{title} AKA {alternate_title}'
    else:
        title_string = title
    if tv:
        title_string += f' - {"Seasons" if multi_season else "Season"} {f"{seasons[0]}-{seasons[-1]}" if multi_season else seasons[0]}'
    title_string += f' ({year})'
    if edition:
        title_string += f' {edition}'
    if complete and tv:
        title_string += ' Complete Series'
    return title_string


def is_latin_char(title: str) -> bool:
    """Determines whether or not the input string contains only latin characters.
        https://stackoverflow.com/a/3308844

    Args:
        title (str): The string to check.

    Returns:
        bool: True if the input string contains only latin characters.
    """
    latin_letters = {}

    def is_latin(uchr):
        try:
            return latin_letters[uchr]
        except KeyError:
            return latin_letters.setdefault(uchr, 'LATIN' in ud.name(uchr))

    def only_roman_chars(unistr):
        # isalpha suggested by John Machin
        return all(is_latin(uchr) for uchr in unistr if uchr.isalpha())

    return only_roman_chars(title)


def copy_to_clipboard(presentation: str) -> bool:
    """Cross-platform method of copying text to clipboard.
        https://stackoverflow.com/a/4203897

    Args:
        presentation (str): The text to be copied to the clipboard.
    """
    try:
        r = Tk()
        r.withdraw()
        r.clipboard_clear()
        r.clipboard_append(presentation)
        r.update()  # now it stays on the clipboard after the window is closed
        r.destroy()
        return True
    except:
        return False


if __name__ == '__main__':
    pass
