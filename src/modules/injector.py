import xmlrpc.client
import ssl
from pathlib import Path
import shutil

from qbittorrent import Client


class Injector:
    def __init__(self, config: dict) -> None:
        self._type = config['injection']['type']
        if self._type not in ['qbittorrent', 'rtorrent', 'watch']:
            raise ValueError(f'Client injection type {self._type} not supported.')
        if self._type == 'qbittorrent':
            self._qb_webui = config['injection']['qbittorrent']['webui']
            self._qb_user = config['injection']['qbittorrent']['user']
            self._qb_pass = config['injection']['qbittorrent']['password']
            self._qb_ssl = config['injection']['qbittorrent']['ssl']
            self._qb = Client(self._qb_webui, verify=self._qb_ssl)
            self._qb.login(self._qb_user, self._qb_pass)
        elif self._type == 'rtorrent':
            self._rtorrent_xmlrpc = config['injection']['rtorrent']
            self._rtorrent = xmlrpc.client.Server(self._rtorrent_xmlrpc, context=ssl.create_default_context())
        elif self._type == 'watch':
            self._watch_path = Path(config['injection']['watch'])
            if not self._watch_path.is_dir():
                raise FileNotFoundError(f'Provided watch folder {self._watch_path} does not seem to exist.')

    def inject(self, torrent_file: Path, torrent_path: Path) -> None:
        if self._type == 'qbittorrent':
            self._qb_inject(torrent_file, torrent_path)
        elif self._type == 'rtorrent':
            self._rtorrent_inject(torrent_file, torrent_path)
        elif self._type == 'watch':
            self._watch_inject(torrent_file)

    def _qb_inject(self, torrent_file: Path, torrent_path: Path) -> None:
        with torrent_file.open('rb') as fb:
            self._qb.download_from_file(fb, savepath=str(torrent_path), label='crt-assistant' )

    def _rtorrent_inject(self, torrent_file: Path, torrent_path: Path) -> None:
        pass

    def _watch_inject(self, torrent_file: Path) -> None:
        shutil.copy(torrent_file, self._watch_path)